#define POWER_PIN P1_4
#define buttonPin P1_3
#define gLed P2_0
#define rLed P1_0



#define REPLY_TIOMEOUT 7000
#define SMS_TRYOUT_TIMES 3
#define CONN_TRYOUT_TIMES 10
#define REG_TRYOUT_TIMES 240


char connection[]="OK";
char serialNum1[]="S007";
char serialNum2[]="H007";
int temp = 7;
char indicators[] = "+CIND: ";
char smsSent[] = "\n+CMGS:";
char b,s,r;

unsigned char buffer[64];
int count=0;
bool indicatorFlag = false;
bool smsFlag = false;
bool endProgrammeFlag = false;
bool isbuttonPressed = false;
void powerDown(void);
void powerUp();


void setup() {
  
  Serial.begin(9600);
 // Serial.begin(9600);
  delay(100);       



  pinMode(gLed, OUTPUT); 
  pinMode(rLed, OUTPUT); 
  pinMode(POWER_PIN, OUTPUT);

  digitalWrite(POWER_PIN, LOW);

  digitalWrite(rLed, LOW);
digitalWrite(gLed, LOW);
  digitalWrite(rLed, HIGH);
  delay(1000);
  digitalWrite(rLed, LOW);

  pinMode(buttonPin,INPUT_PULLUP);
  delay(1000);
  attachInterrupt(buttonPin, wakeUP, CHANGE);
  powerUp();
  sendHearbeat();

}

void loop() {
  digitalWrite(gLed, LOW);
  digitalWrite(rLed, LOW);
  powerDown();
  sleepSeconds(45);
  if(isbuttonPressed){
  powerUp();
  sendSignal();
  digitalWrite(rLed, HIGH);
 
 // digitalWrite(gLed, LOW);
  //digitalWrite(gLed, HIGH);
  delay(1000);
  digitalWrite(rLed, LOW);
  isbuttonPressed = false;
}
  else{
    powerUp();
    sendHearbeat();
  }
}
void sendHearbeat(){
    for (int i = 0; i < 6; ++i)
  {
    Serial.println(serialNum2);
    temp = (temp*7)% 97;
    for(int j=0;j < temp;j++)
      delay(2);
  }
}
void wakeUP(){
  isbuttonPressed = true;
  wakeup();
}

void powerUp(){
digitalWrite(POWER_PIN, HIGH);
delay(100);

}

void powerDown(){
digitalWrite(POWER_PIN, LOW);
delay(200);
}
void sendSignal(){
  for (int i = 0; i < 10; ++i)
  {
    Serial.println(serialNum1);
    temp = (temp*7)% 97;
    for(int j=0;j < temp;j++)
      delay(2);
  }
}




void DebugPrint(String msg){
  //Serial.println(msg);
}

